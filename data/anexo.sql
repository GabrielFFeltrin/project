CREATE TABLE `anexo` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`nome` VARCHAR(255) NOT NULL,
	`mime` VARCHAR(255) NOT NULL,
	`data` BLOB NOT NULL,
	`id_msg` INT(11) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `id_msg` (`id_msg`),
	CONSTRAINT `id_msg` FOREIGN KEY (`id_msg`) REFERENCES `mensagem` (`id`)
);