$(document).ready(function () {
    $("#foto").change(function () {
        $.ajax({
            url: '/../treinamento/projeto/Controllers/foto.php',
            type: 'POST',
            data: new FormData(document.getElementById('cadastroCliente')),
            processData: false,
            contentType: false,
            success: function (data) {
                $("#imagem").html(data);
            }
        })
    });
    $("#salvarCliente").click(function () {
        $.ajax({
            url: '/../treinamento/projeto/Controllers/insertCliente.php',
            type: 'POST',
            data: new FormData(document.getElementById('cadastroCliente')),
            processData: false,
            contentType: false,
            error: function (data) {
                $("#form-message").html("Página não encontrada");
            },
            success: function (data) {
                $("#mensagem").html(data);
            }
        });
    });
});
