$(document).ready(function () {
	$.ajax({
		url: '/../treinamento/projeto/Controllers/lista.php',
		type: 'POST',
		data: {
			tipo: $("#tipo").val(),
		},
		error: function (data) {
			$("#mensagem").html("Página não encontrada");
		},
		success: function (data) {
			$("#result").html(data);
		}
	});
	$("#adicionar").click(function () {
		var tipo = $(this).val();
		if(tipo == 'func'){
			window.location.href='/../treinamento/projeto/view/funcionario/cadastroFunc.php'
		}else if(tipo == 'cliente'){
			window.location.href='/../treinamento/projeto/view/cliente/cadastroCliente.php'
		}else if(tipo == 'empresa'){
			window.location.href='/../treinamento/projeto/view/empresa/cadastroEmpresa.php'
		}
		
    });
});
