<?php
require_once __DIR__.'/../conexao.php';
require_once __DIR__.'/Listagem.php';

$lista = new Listagem($db);

$tipo = $_POST['tipo'];

if ($tipo == 'func') {
	$buscar = "SELECT * FROM `projeto`.`crmfunc` ORDER BY `nomeUsuario`";
	$lista->func($buscar);
}elseif($tipo == 'cliente'){
	$buscar = "SELECT * FROM `projeto`.`cliente` ORDER BY `nomeUsuario`";
	$lista->cliente($buscar);
}elseif($tipo == 'empresa'){
	$buscar = "SELECT * FROM `projeto`.`empresa` ORDER BY `nome`";
	$lista->empresa($buscar);
}