$(document).ready(function () {
	var codigo = $("#codigo").val();
	$.ajax({
		url: '/../treinamento/projeto/Controllers/show.php',
		type: 'POST',
		data: {
			codigo: codigo
		},
		error: function (data) {
			$("#mensagem").html("Página não encontrada");
		},
		success: function (data) {
			$('#result').html(data);
		},
	});
	$("#finalizar").click(function () {
		$.ajax({
			url: '/../treinamento/projeto/Controllers/finalizar.php',
			type: 'POST',
			data: {
				codigo: codigo,
			},
			error: function (data) {
				$("#mensagem").html("Página não encontrada");
			},
			success: function (data) {
				$("#mensagem").html(data);
			},
		});
	});
	$("#voltarCliente").click(function () {
		window.location.replace('/../treinamento/projeto/view/chamado/chamadoCliente.php')
	});
	$("#voltarFunc").click(function () {
		window.location.replace('/../treinamento/projeto/view/chamado/chamado.php')
	});
	$("#mensagens").click(function () {
		window.location.replace('/../treinamento/projeto/view/chamado/mensagem.php')
	});
	
	
})
