<?php

class Listagem
{
    private $db;

    public function __construct(Conn $db)
    {
        $this->db = $db->connect();
    }
    
    public function func($query)
    {
        $sqlSearch = $this->db->prepare($query);
        $sqlSearch->execute();

        $lista = $sqlSearch->fetchAll(\PDO::FETCH_ASSOC);

        echo"   <thead>
                    <tr>
                        <td>ID</td>
						<td>Nome de Usuário</td>
						<td>Email</td>
                        <td>Editar</td>
                        <td>Excluir</td>
                    </tr>
                </thead>
				<tbody>";
        if ($lista == null) {
            echo "<td><span class='text-danger lead font-weight-bold'>Nenhum registro encontrado</span></td>";
        } else {
            foreach ($lista as $row) {
                $id = $row['id'];
                echo "<tr>
					<td> $id </td>
					<td> ".$row['nomeUsuario']." </td>
                    <td> ".$row['email'] ."</td>
					<td><a class=' text-primary lead font-weight-bold' href='/../treinamento/projeto/view/funcionario/editarFunc.php?id=".$id."'>Editar</a></td>
					<td><a onClick=\"javascript: return confirm('Confirma exclusão?');\" class='text-danger lead font-weight-bold' href='/../treinamento/projeto/Controllers/deleteFunc.php?id=".$id."'>Excluir</a></td>
				  </tr>";
            };
        }
        echo "</tbody>";
    }

    public function cliente($query)
    {
        $sqlSearch = $this->db->prepare($query);
        $sqlSearch->execute();

        $lista = $sqlSearch->fetchAll(\PDO::FETCH_ASSOC);

        echo"   <thead>
                    <tr>
                        <td>ID</td>
						<td>Nome de Usuário</td>
						<td>Email</td>
                        <td>Empresa</td>
                        <td>Editar</td>
                        <td>Excluir</td>
                    </tr>
                </thead>
				<tbody>";
        if ($lista == null) {
            echo "<td><span class='text-danger lead font-weight-bold'>Nenhum registro encontrado</span></td>";
        } else {
            foreach ($lista as $row) {
                $id = $row['id'];

                $empresa = $row['id_empresa'];
                $query = "SELECT `nome` FROM `projeto`.`empresa` WHERE `id` = '$empresa'";
                $empresa = $this->db->prepare($query);
                $empresa->execute();
                $empresa = $empresa->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT);
                $empresa = $empresa[0];

                echo "<tr>
					<td> $id </td>
					<td> ".$row['nomeUsuario']." </td>
					<td> ".$row['email'] ."</td>
                    <td> ".$empresa."</td>
					<td><a class='text-primary lead font-weight-bold' href='/../treinamento/projeto/view/cliente/editarCliente.php?id=".$id."'>Editar</a></td>
					<td><a onClick=\"javascript: return confirm('Confirma exclusão?');\" class='text-danger lead font-weight-bold' href='/../treinamento/projeto/Controllers/deleteCliente.php?id=".$id."'>Excluir</a></td>
				  </tr>";
            };
        }
        echo "</tbody>";
    }

    public function empresa($query)
    {
        $sqlSearch = $this->db->prepare($query);
        $sqlSearch->execute();

        $lista = $sqlSearch->fetchAll(\PDO::FETCH_ASSOC);

        echo"   
				<thead>
                    <tr>
                        <td>ID</td>
						<td>Nome</td>
                        <td>CNPJ</td>
						<td>Editar</td>
						<td>Excluir</td>
                    </tr>
                </thead>
				<tbody>";
        if ($lista == null) {
            echo "<td><span class='text-danger lead font-weight-bold'>Nenhum registro encontrado</span></td>";
        } else {
            foreach ($lista as $row) {
                $id = $row['id'];
                echo "<tr>
					<td> $id </td>
					<td> ".$row['nome']." </td>
                    <td> ".$row['cnpj'] ."</td>
					<td><a class='text-primary lead font-weight-bold' href='/../treinamento/projeto/view/empresa/editarEmpresa.php?id=".$id."'>Editar</a></td>
					<td><a onClick=\"javascript: return confirm('Confirma exclusão?');\" class='text-danger lead font-weight-bold' href='/../treinamento/projeto/Controllers/deleteEmpresa.php?id=".$id."'>Excluir</a></td>
				  </tr>";
            };
        }
        echo "</tbody>
		<script src='/../treinamento/projeto/js/empresa.js'></script>";
    }
}
