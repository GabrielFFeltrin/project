<?php


class AnexoControllers
{
    private $db;


    public function __construct(Conn $db)
    {
        $this->db = $db->connect();
    }

    public function salvar(Anexo $anexo)
    {
        $mime = $anexo->getMime();
        $data = $anexo->getData();
        $msg = $anexo->getMsg();
        $nome = $anexo->getNome();
        $sqlInsert = $this->db->prepare("INSERT INTO `projeto`.`anexo` (`nome`,`mime`,`data`,`id_msg`) VALUES (?,?,?,'$msg')");
        $sqlInsert->bindParam(1, $nome);
        $sqlInsert->bindParam(2, $mime);
        $sqlInsert->bindParam(3, $data);

        $sqlInsert->execute();
    }

    public function anexo($id)
    {
        $sqlSearch = $this->db->prepare("SELECT * FROM `projeto`.`anexo` WHERE `id_msg` = '$id'");
        $sqlSearch->execute();

        $lista = $sqlSearch->fetchAll(\PDO::FETCH_ASSOC);

        $i=0;
        foreach ($lista as $row) {
            echo "<a href='/../treinamento/projeto/Controllers/download.php?id=".$row['id']."' target='_blank'>".$row['nome']."</a><br>";
            $i++;
        }
    }

    public function excluir($mensagem)
    {
        $sqlDelete = $this->db->prepare("DELETE FROM `projeto`.`anexo` WHERE `id_msg` = '$mensagem'");
        $sqlDelete->execute();
        $sqlDelete->fetchAll(\PDO::FETCH_ASSOC);
    }
}
