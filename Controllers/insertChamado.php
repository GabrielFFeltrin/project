<?php
    require_once __DIR__.'/../session.php';
    require_once __DIR__.'/../OO/Chamado.php';
    require_once __DIR__.'/../OO/Mensagem.php';
    require_once __DIR__.'/../OO/Anexo.php';
    require_once __DIR__.'/ChamadoControllers.php';
    require_once __DIR__.'/MensagemControllers.php';
    require_once __DIR__.'/AnexoControllers.php';

    $chamado = new Chamado();
    $mensagem = new Mensagem();
    $anexo = new Anexo();

    $i = 0;
    if (!isset($FILES)) {
        $anexos = '';
    }

    require_once __DIR__.'/../conexao.php';

    $date = date('c');
    $user = $_SESSION['user']['user'];

    $func = $conn->prepare("SELECT * FROM `projeto`.`crmfunc` WHERE `nomeUsuario` = '$user'");
    $func->execute();
    $resultFunc = $func->fetch(\PDO::FETCH_ASSOC);
    $cliente = $conn->prepare("SELECT * FROM `projeto`.`cliente` WHERE `nomeUsuario` = '$user'");
    $cliente->execute();
    $resultCliente = $cliente->fetch(\PDO::FETCH_ASSOC);
    if ($resultFunc != null) {
        $chamado->setUsuario($resultFunc['nomeUsuario']);
    } elseif ($resultCliente != null) {
        $chamado->setUsuario($resultCliente['nomeUsuario']);
    } 
    


    $chamado->setData($date);
    $chamado->setStatus(0);
    $chamado->setAssunto($_POST['assunto']);



    $mensagem->setUser($user);
    $mensagem->setMensagem($_POST['desc']);
    $mensagem->setData($date);
    if ($_POST['desc'] == '') {
        echo "<span class='text-danger lead font-weight-bold'>Mensagem/Descrição obrigatória</span>";
    } elseif ($_POST['assunto']=='') {
        echo "<span class='text-danger lead font-weight-bold'>Assunto obrigatório</span>";
    } else {
        $insert = new ChamadoControllers($db);
        $msg = new MensagemControllers($db);
        $files = new AnexoControllers($db);

        $insert->salvar($chamado, $msg, $mensagem, $anexo);

        $msg->salvar($mensagem, $anexo);
        foreach ($_FILES as $key) {
            $nome = $key['name'];
            $type = $key['type'];
            $anexos = explode('.', $key['name']);
            if ($anexos[1] != 'exe') {
                $anexos = file_get_contents($key['tmp_name']);
                $anexo->setNome($nome);
                $anexo->setMime($type);
                $anexo->setData($anexos);
                $files->salvar($anexo);
            } else {
                echo "<span class='text-danger lead font-weight-bold'>Proibido upload de arquivos potêncialmente maliciosos</span>";
            }
            $i++;
        }
        echo "<span class='text-success lead font-weight-bold'>Chamado aberto</span>";

        
    }
