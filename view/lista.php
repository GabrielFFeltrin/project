<?php

require __DIR__.'/../session.php';
require __DIR__.'/../conexao.php';
if (!isset($_SESSION['user']['user'])) {
    echo "<script>alert('Acesso Negado')
    window.location.replace('/../treinamento/projeto/view/index.php')</script>";
}
if (!isset($_SESSION['chamado']['chamado'])) {
    require __DIR__.'/../Controllers/visualiza.php';
}

$func = $conn->prepare("SELECT * FROM `projeto`.`crmfunc` WHERE `nomeUsuario` = '".$_SESSION['user']['user']."'");
$func->execute();
$resultFunc = $func->fetch(\PDO::FETCH_ASSOC);

if ($resultFunc == null) {
    echo "<script>alert('Acesso Negado')
    window.location.replace('/../treinamento/projeto/view/index.php')</script>";
}

$chamado = $conn->prepare("SELECT `status` FROM `projeto`.`chamado` WHERE `codigo` = '".$_SESSION['chamado']['chamado']."'");
$chamado->execute();
$status = $chamado->fetch(\PDO::FETCH_ASSOC);

$tipo = $_GET['tipo'];

if($tipo == 'func'){
    $listagem = 'Funcionários';
}elseif($tipo == 'cliente'){
    $listagem = 'Clientes';
}elseif($tipo == 'empresa'){
    $listagem = 'Empresas';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cadastro de Cliente</title>
    <link rel="stylesheet" href="/../treinamento/projeto/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="/../treinamento/projeto/downloads/fontAwesome/css/all.min.css"/>
    <script src="/../treinamento/projeto/jquery/jquery.js"></script>
    <link rel="stylesheet" href="/../treinamento/projeto/jquery/jquery-ui-1.12.1/jquery-ui.css"/>
    
    <script src="/../treinamento/projeto/jquery/jquery-ui-1.12.1/jquery-ui.js"></script>
</head>
<body>
    <div class="container-fluid" id="container">
        <div class="row flex-row justify-content-start h-100">
            <aside class="col-12 p-0 mh-100 bg-primary">
                <nav class="navbar navbar-expand-lg navbar-light bg-primary border-bottom">
                    <a class="navbar-brand order-0 order-lg-0 mr-lg-0 mr-2 " href="/../treinamento/projeto/view/<?php if ($resultFunc != null) {
							echo "indexFunc.php";
						} elseif ($resultCliente != null) {
							echo "indexCliente.php";
						}?>"><i class="fas fa-home mr-2"></i></a>
                    <a class="navbar-brand order-0 order-lg-0 ml-lg-0 ml-2 mr-auto ml-auto"><h4 class='text-light'>Olá, <?php echo $_SESSION['user']['user']; ?></h4></a>
                    <a class="navbar-brand order-0 order-lg-0 ml-lg-5 "><h4 class='text-light'>Listagem de <?php echo $listagem?></h4></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                            <li class="nav-item dropdown mr-4">
                                <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Cadastros
                                </a>
                                <div class="dropdown-menu bg-primary" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item text-light text-wrap" href="/../treinamento/projeto/view/lista.php?tipo=func">Funcionários</a>
                                <a class="dropdown-item text-light" href="/../treinamento/projeto/view/lista.php?tipo=cliente">Clientes</a>
                                <a class="dropdown-item text-light" href="/../treinamento/projeto/view/lista.php?tipo=empresa">Empresas</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown mr-4">
                                <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Chamados
                                </a>
                                <div class="dropdown-menu bg-primary" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item text-light text-wrap" href="/../treinamento/projeto/view/chamado/novoChamado.php">Abrir Chamado</a>
                                <a class="dropdown-item text-light text-wrap" href="/../treinamento/projeto/view/chamado/chamado.php">Gerenciar Chamados</a>
                                </div>
                            </li>
                            <li class="nav-item">
                            <a class="text-light nav-link" href="/../treinamento/projeto/logout.php">Sair</a> 
                            </li>
                        </ul>
                    </div>
                </nav>
            </aside>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-10 col-md-8 col-sm-6 col-6"></div>
                    <div class="col-lg-2 col-md-4 col-sm-6 col-6"><button type = 'button' id="adicionar" class='btn btn-primary col-12 my-5' value="<?php echo $tipo; ?>">Adicionar</button></div>
                </div>
                
                <aside class="col-6 col-md-12">
                    <form id="lista">
                        <input type="hidden" id="tipo" value="<?php echo $tipo; ?>">
                        <table id="result" class="table">
                        </table>
                        
                        <div id= "mensagem"></div>
                    </form>
                </aside>    
            </div>
        </div>
    </div>
    <script src="/../treinamento/projeto/js/lista.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="/../treinamento/projeto/js/bootstrap.bundle.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>