<?php


class Anexo
{
    private $id;
    private $nome;
    private $data;
    //private $anexo;
    private $mime;
    private $msg;



    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @param mixed $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
    

    /**
     * @param mixed $mime
     */
    public function setMime($mime): void
    {
        $this->mime = $mime;
    }

    /**
     * @return mixed
     */
    public function getMime()
    {
        return $this->mime;
    }

    

    /**@return mixed

    public function getAnexo()
    {
        return $this->anexo;
    }


     * @param mixed $anexo

    public function setAnexo($anexo): void
    {
        $this->anexo = $anexo;
    }*/

    /**@return mixed
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * @param mixed $msg
     */
    public function setMsg($msg): void
    {
        $this->msg = $msg;
    }
}
