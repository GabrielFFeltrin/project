CREATE TABLE `chamado` (
	`codigo` INT(11) NOT NULL AUTO_INCREMENT,
	`assunto` VARCHAR(255) NOT NULL,
	`data` DATE NOT NULL,
	`status` INT(11) NOT NULL,
	`usuario` VARCHAR(255) NOT NULL DEFAULT '',
	PRIMARY KEY (`codigo`)
);