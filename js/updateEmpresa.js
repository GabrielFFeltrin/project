$(document).ready(function () {
    $("#atualizarEmpresa").click(function () {
        $.ajax({
            url: '/../treinamento/projeto/Controllers/updateEmpresa.php',
            type: 'POST',
            data: new FormData(document.getElementById('atualizaEmpresa')),
            processData: false,
            contentType: false,
            error: function (data) {
                $("#mensagem").html("Página não encontrada");
            },
            success: function (data) {
                $("#mensagem").html(data);
            }
        });
    });
});