CREATE TABLE `mensagem` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`id_chamado` INT(11) NOT NULL,
	`mensagem` VARCHAR(500) NOT NULL,
	`data` DATE NOT NULL,
	`user` VARCHAR(255) NOT NULL DEFAULT '',
	`novo` INT(11) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `id_chamado` (`id_chamado`),
	CONSTRAINT `id_chamado` FOREIGN KEY (`id_chamado`) REFERENCES `chamado` (`codigo`)
);