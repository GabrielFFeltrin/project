<?php
include __DIR__ . '/../session.php';
include __DIR__ . '/../OO/Person.php';
include __DIR__ . '/../OO/Fisico.php';
include __DIR__ . '/../OO/Cliente.php';
include_once __DIR__ . '/ClienteControllers.php';


$cliente = new Cliente();

$antigoUser = $_POST['antigoUser'];
$user       = $_POST['user'];
$email      = $_POST['email'];
$senha      = $_POST['pass'];

$validaArquivo = 0;

require_once __DIR__ . '/../conexao.php';



$query = "SELECT * FROM `projeto`.`cliente` WHERE `nomeUsuario` = '$antigoUser'";
$searchCliente = $conn->prepare($query);
$searchCliente->execute();
$searchCliente = $searchCliente->fetch(\PDO::FETCH_ASSOC);
$id = $searchCliente['id'];
$foto = $searchCliente['foto'];

if($searchCliente['foto'] != ''){
    $foto = $searchCliente['foto'];;
}

if ($_FILES["foto"]["tmp_name"] != '') {
    $foto = explode('.', $_FILES['foto']['name']);
    if ($foto[1] != 'exe') {
        $foto = file_get_contents($_FILES['foto']['tmp_name']);
    } else {
        $validaArquivo = 1;
    }
}else{
    
    $foto = '';
}

$validaEmail="SELECT `email` FROM `projeto`.`cliente` WHERE `email` = '$email' AND `id` != '$id'";
$validaEmail = $conn->prepare($validaEmail);
$validaEmail->execute();
$validaEmail1 = $validaEmail->fetch(PDO::FETCH_ASSOC);

$validaUser="SELECT `nomeUsuario` FROM `projeto`.`cliente` WHERE `nomeUsuario` = '$antigoUser' AND `id` != '$id'";
$validaUser = $conn->prepare($validaUser);
$validaUser->execute();
$validaUser1 = $validaUser->fetch(PDO::FETCH_ASSOC);


$validaEmail="SELECT `email` FROM `projeto`.`crmfunc` WHERE `email` = '$email'";
$validaEmail = $conn->prepare($validaEmail);
$validaEmail->execute();
$validaEmail2 = $validaEmail->fetch(PDO::FETCH_ASSOC);

$validaUser="SELECT `nomeUsuario` FROM `projeto`.`crmfunc` WHERE `nomeUsuario` = '$user'";
$validaUser = $conn->prepare($validaUser);
$validaUser->execute();
$validaUser2 = $validaUser->fetch(PDO::FETCH_ASSOC);

if ($user == '') {
    echo "<span class='text-danger lead font-weight-bold'>Nome obrigatório</span>";
} elseif ($validaUser1['nomeUsuario'] == $user || $validaUser2['nomeUsuario'] == $user) {
    echo "<span class='text-danger lead font-weight-bold'>Nome indisponível</span>";
} elseif ($email == '') {
    echo "<span class='text-danger lead font-weight-bold'>Email obrigatório</span>";
} elseif ($validaEmail1['email'] == $email || $validaEmail2['email'] == $email) {
    echo "<span class='text-danger lead font-weight-bold'>Email indisponível</span>";
} elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    echo "<span class='text-danger lead font-weight-bold'>Email inválido</span>";
} elseif ($validaArquivo) {
    echo "<span class='text-danger lead font-weight-bold'>Proibido upload de arquivos potêncialmente maliciosos</span>";
} else {
    $cliente->setFoto($foto);
    $cliente->setNome($user);
    $cliente->setEmail($email);
    if ($senha == '') {
        $senha="SELECT `password` FROM `projeto`.`cliente` WHERE `nomeUsuario` = '$antigoUser'";
        $senha = $conn->prepare($senha);
        $senha->execute();
        $senha = $senha->fetch(PDO::FETCH_ASSOC);
        $senha = $senha['password'];
    } 
    $cliente->setSenha($senha);

    $update = new ClienteControllers($db, $cliente);

    $update->update($id);


    $query = "SELECT `id` FROM `projeto`.`crmfunc` WHERE `nomeUsuario` = '".$_SESSION['user']['user']."'";
    $func = $conn->prepare($query);
    $func->execute();
    $func = $func->fetch(\PDO::FETCH_ASSOC);
    $func = $func['id'];

    echo "<span class='text-success lead font-weight-bold'>Atualizado com sucesso</span>";
    $_SESSION['user']['user'] = $user;
    
}
