$(document).ready(function () {
    $.ajax({
        url: "/../treinamento/projeto/Controllers/search.php",
        type: "POST",
        data: {
            codigo:  '',
            date1:   '',
            date2:   '',
        },
        success: function (data) {
            $('#listTable').html(data);
        }
    })

    $("#buscarChamado").click(function () {
        $.ajax({
            url: "/../treinamento/projeto/Controllers/search.php",
            type: "POST",
            data: {
                codigo: $("#codigo").val(),
                date1: $("#data1").val(),
                date2: $("#data2").val(),
            },
            success: function (data) {
                $('#listTable').html(data);
            }
        })
    });
});
