<?php

include __DIR__ . '/../OO/Person.php';
include __DIR__ . '/../OO/Fisico.php';
include __DIR__ . '/../OO/Cliente.php';
include_once __DIR__ . '/ClienteControllers.php';

$cliente = new Cliente();
$user = $_POST['user'];
$email = $_POST['email'];
$senha = $_POST['pass'];
$empresa = $_POST['empresa'];

$validaArquivo = 0;

if ($_FILES["foto"]["tmp_name"] != '') {
    $foto = explode('.', $_FILES['foto']['name']);
    if ($foto[1] != 'exe') {
        $foto = file_get_contents($_FILES['foto']['tmp_name']);
    } else {
        $validaArquivo = 1;
    }
} else {
    $foto='';
}


require_once __DIR__.'/../conexao.php';

$validaEmail="SELECT `email` FROM `projeto`.`cliente` WHERE `email` = '$email'";
$validaEmail = $conn->prepare($validaEmail);
$validaEmail->execute();
$validaEmail1 = $validaEmail->fetch(PDO::FETCH_ASSOC);

$validaUser="SELECT `nomeUsuario` FROM `projeto`.`cliente` WHERE `nomeUsuario` = '$user'";
$validaUser = $conn->prepare($validaUser);
$validaUser->execute();
$validaUser1 = $validaUser->fetch(PDO::FETCH_ASSOC);


$validaEmail="SELECT `email` FROM `projeto`.`crmfunc` WHERE `email` = '$email'";
$validaEmail = $conn->prepare($validaEmail);
$validaEmail->execute();
$validaEmail2 = $validaEmail->fetch(PDO::FETCH_ASSOC);

$validaUser="SELECT `nomeUsuario` FROM `projeto`.`crmfunc` WHERE `nomeUsuario` = '$user'";
$validaUser = $conn->prepare($validaUser);
$validaUser->execute();
$validaUser2 = $validaUser->fetch(PDO::FETCH_ASSOC);

if ($user == '') {
    echo "<span class='text-danger lead font-weight-bold'>Nome obrigatório</span>";
} elseif ($validaUser1['nomeUsuario'] == $user || $validaUser2['nomeUsuario'] == $user) {
    echo "<span class='text-danger lead font-weight-bold'>Nome indisponível</span>";
} elseif ($email == '') {
    echo "<span class='text-danger lead font-weight-bold'>Email obrigatório</span>";
} elseif ($validaEmail1['email'] == $email || $validaEmail2['email'] == $email) {
    echo "<span class='text-danger lead font-weight-bold'>Email indisponível</span>";
} elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    echo "<span class='text-danger lead font-weight-bold'>Email inválido</span>";
} elseif ($empresa == '') {
    echo "<span class='text-danger lead font-weight-bold'>Empresa obrigatória</span>";
} elseif ($validaArquivo) {
    echo "<span class='text-danger lead font-weight-bold'>Proibido upload de arquivos potêncialmente maliciosos</span>";
} else {
    $cliente->setEmpresa($empresa);
    $cliente->setFoto($foto);
    $cliente->setNome($user);
    $cliente->setEmail($email);
    $cliente->setSenha($senha);
  
    $insert = new ClienteControllers($db, $cliente);

    $insert->salvar();
    echo "<span class='text-success lead font-weight-bold'>Cadastrado com sucesso</span>";
}
