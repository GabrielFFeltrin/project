$(document).ready(function () {
    $("#salvarEmpresa").click(function () {
        $.ajax({
            url: '/../treinamento/projeto/Controllers/insertEmpresa.php',
            type: 'POST',
            data: {
                cnpj: $("#cnpj").val(),
                nome: $("#nome").val()
            },
            error: function (data) {
                $("#mensagem").html("Página não encontrada");
            },
            success: function (data) {
                $("#mensagem").html(data);
            }
        });
    });
});

