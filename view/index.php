<?php

include_once __DIR__ . '/../conexao.php';


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="/../treinamento/projeto/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="/../treinamento/projeto/downloads/fontAwesome/css/all.min.css"/>
    <script src="/../treinamento/projeto/jquery/jquery.js"></script>
    <link rel="stylesheet" href="/../treinamento/projeto/jquery/jquery-ui-1.12.1/jquery-ui.css"/>
    
    <script src="/../treinamento/projeto/jquery/jquery-ui-1.12.1/jquery-ui.js"></script></head>
<body>
    <div class="container-fluid">
        <div class="col-12">
            <form id="cadastroCliente">
                <div class="form-group offset-lg-4 col-lg-4 col-sm-12 pt-4 p-0">
                    <label for="user">Nome:</label>
                    <input type="text" id="user" name="user" placeholder="Nome" class="form-control"/>
                </div>
                <div class="form-group offset-lg-4 col-lg-4 col-sm-12 pt-4 p-0">
                    <label for="pass">Senha:</label>
                    <input type="password" id="pass" name="pass" placeholder="Senha" class="form-control"/>
                </div>
                <div class="form-group offset-lg-4 col-lg-4 col-sm-12 pt-3 text-right pr-0">
                    <button class="btn btn-danger" type="reset">Cancelar</button>
                    <button class="btn btn-primary "type="button" id="login">Enviar</button>
                </div>
                <div class="form-group offset-lg-4 col-lg-4 col-sm-12 pt-3 text-center pr-0" id="permission"></div>
            </form>
        </div>
    </div>
    <script src="/../treinamento/projeto/js/acesso.js"></script>
</body>
</html>