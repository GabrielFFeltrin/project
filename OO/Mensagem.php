<?php


class Mensagem
{
    private $id;
    private $mensagem;
    private $data;
    private $chamado;
    private $user;
    private $novo;



    /**
     * @return mixed
     */
    public function getChamado()
    {
        return $this->chamado;
    }

    /**
     * @param mixed $chamado
     */
    public function setChamado($chamado): void
    {
        $this->chamado = $chamado;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNovo()
    {
        return $this->novo;
    }

    /**
     * @param mixed $novo
     */
    public function setNovo($novo): void
    {
        $this->novo = $novo;
    }

    /**
     * @return mixed
     */
    public function getMensagem()
    {
        return $this->mensagem;
    }

    /**
     * @param mixed $mensagem
     */
    public function setMensagem($mensagem): void
    {
        $this->mensagem = $mensagem;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }


    /**@return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }
}
