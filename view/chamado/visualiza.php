<?php

require __DIR__.'/../../session.php';
require __DIR__.'/../../conexao.php';
if (!isset($_SESSION['user']['user'])) {
    echo "<script>alert('Acesso Negado')
    window.location.replace('/../treinamento/projeto/view/index.php')</script>";
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $_SESSION['chamado'] = [
        'chamado' => filter_input(INPUT_GET, 'codigo')
    ];
}


$func = $conn->prepare("SELECT * FROM `projeto`.`crmfunc` WHERE `nomeUsuario` = '".$_SESSION['user']['user']."'");
$func->execute();
$resultFunc = $func->fetch(\PDO::FETCH_ASSOC);

$cliente = $conn->prepare("SELECT * FROM `projeto`.`cliente` WHERE `nomeUsuario` = '".$_SESSION['user']['user']."'");
$cliente->execute();
$resultCliente = $cliente->fetch(\PDO::FETCH_ASSOC);

$chamado = $conn->prepare("SELECT * FROM `projeto`.`chamado` WHERE `codigo` = '".$_SESSION['chamado']['chamado']."'");
$chamado->execute();
$status = $chamado->fetch(\PDO::FETCH_ASSOC);

if($resultFunc == null && $status['usuario'] != $_SESSION['user']['user']){
    echo "<script>alert('Acesso Negado')
    window.location.replace('/../treinamento/projeto/view/index.php')</script>";
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cadastro de Cliente</title>
    <link rel="stylesheet" href="/../treinamento/projeto/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="/../treinamento/projeto/downloads/fontAwesome/css/all.min.css"/>
    <script src="/../treinamento/projeto/jquery/jquery.js"></script>
    <link rel="stylesheet" href="/../treinamento/projeto/jquery/jquery-ui-1.12.1/jquery-ui.css"/>
    
    <script src="/../treinamento/projeto/jquery/jquery-ui-1.12.1/jquery-ui.js"></script>
</head>
<body>
    <div class="container-fluid" id="container">
        <div class="row flex-row justify-content-start h-100">
            <aside class="col-12 p-0 mh-100 bg-primary">
                <nav class="navbar navbar-expand-lg navbar-light bg-primary border-bottom">
                    <a class="navbar-brand order-0 order-lg-0 mr-lg-0 mr-2 " href="/../treinamento/projeto/view/<?php if ($resultFunc != null) {
                            echo "indexFunc.php";
                        } elseif ($resultCliente != null) {
                            echo "indexCliente.php";
                        }?>"><i class="fas fa-home mr-2"></i>
                    </a>
                    <a class="navbar-brand order-0 order-lg-0 ml-lg-0 ml-2 mr-auto ml-auto"><h4 class='text-light'>Olá, <?php echo $_SESSION['user']['user']; ?></h4></a>
                    <a class="navbar-brand order-0 order-lg-0 ml-lg-5 "><h4 class='text-light'>Chamado</h4></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                            <li class="nav-item dropdown mr-4">
                                <?php if ($resultCliente['id'] == '') {
                                    echo"
                                    <li class='nav-item dropdown mr-4'>
                                        <a class='nav-link dropdown-toggle text-light' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                        Cadastros
                                        </a>
                                        <div class='dropdown-menu bg-primary' aria-labelledby='navbarDropdown'>
                                        <a class='dropdown-item text-light text-wrap' href='/../treinamento/projeto/view/lista.php?tipo=func'>Funcionários</a>
                                        <a class='dropdown-item text-light' href='/../treinamento/projeto/view/lista.php?tipo=cliente'>Clientes</a>
                                        <a class='dropdown-item text-light' href='/../treinamento/projeto/view/lista.php?tipo=empresa'>Empresas</a>
                                        </div>
                                    </li>";
                                }else{
                                    echo"
                                    <li class='nav-item dropdown mr-4'>
                                        <a class='nav-link text-light text-wrap' href='/../treinamento/projeto/view/cliente/editarCliente.php?id=".$resultCliente['id']."'>Editar Perfil</a>
                                    </li>";
                                }?>
                            <li class="nav-item dropdown mr-4">
                                <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Chamados
                                </a>
                                <div class="dropdown-menu bg-primary" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item text-light text-wrap" href="/../treinamento/projeto/view/chamado/novoChamado.php">Abrir Chamado</a>
                                <a class="dropdown-item text-light text-wrap" href="/../treinamento/projeto/view/chamado/<?php if ($resultFunc != null) {
                                        echo "chamado.php";
                                    } elseif ($resultCliente != null) {
                                        echo "chamadoCliente.php";
                                    }?>">Chamados</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="text-light nav-link" href="/../treinamento/projeto/logout.php">Sair</a> 
                            </li>
                        </ul>
                    </div>
                </nav>
            </aside>
            <div class="container-fluid">
                <div class="col-12">
                    <form id="chamado">
                            <input type="hidden" id="codigo" value="<?php echo $_SESSION['chamado']['chamado']; ?>">
                            <table id="result" class="table">
                            </table>
                        <div class="form-group offset-lg-3 col-lg-9 col-sm-12 pt-3 text-right pr-0">
                        <?php
                            if (!$status['status']) {
                                echo "<button class='btn btn-danger mr-1' type='button' id='finalizar'>Finalizar</button>";
                            }

                            echo "<button type='button' class='btn btn-primary mr-1' id='mensagens'>Mensagens</button>";
                            
                            if ($resultFunc == null) {
                                echo "<button type='button' class='btn btn-secondary mr-1' id='voltarCliente'>Chamados</button>";
                            }elseif($resultCliente == null){
                                echo "<button type='button' class='btn btn-secondary mr-1' id='voltarFunc'>Chamados</button>";
                            }
                        ?>
                        </div>
                        <div id= "mensagem" class='text-center'></div>
                    </form>
                </div>    
            </div>
        </div>
    </div>
    <script src="/../treinamento/projeto/js/chamado.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="/../treinamento/projeto/js/bootstrap.bundle.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

</body>
</html>