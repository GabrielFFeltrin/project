<?php
require __DIR__.'/../../session.php';
require __DIR__.'/../../conexao.php';

$user = $_SESSION['user']['user'];
$id = $_GET['id'];
if (!isset($user)) {
    echo "<script>alert('Acesso Negado')
    window.location.replace('/../treinamento/projeto/view/index.php')</script>";
} else {
    $func = $conn->prepare("SELECT `id` FROM `projeto`.`crmfunc` WHERE `nomeUsuario` = '$user'");
    $func->execute();
    $resultfunc = $func->fetch(\PDO::FETCH_ASSOC);

    if ($resultfunc['id'] == '') {
        echo "<script>alert('Acesso Negado')
        window.location.replace('/../treinamento/projeto/logout.php')</script>";
    }
}


$query = "SELECT * FROM `projeto`.`empresa` WHERE `id` = '$id'";
$empresa = $conn->prepare($query);
$empresa->execute();
$empresa = $empresa->fetch(\PDO::FETCH_ASSOC);
$cnpj = $empresa['cnpj'];
$nome = $empresa['nome'];
$id = $empresa['id'];


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cadastro de Cliente</title>
    <link rel="stylesheet" href="/../treinamento/projeto/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="/../treinamento/projeto/downloads/fontAwesome/css/all.min.css"/>
    <script src="/../treinamento/projeto/jquery/jquery.js"></script>
    <link rel="stylesheet" href="/../treinamento/projeto/jquery/jquery-ui-1.12.1/jquery-ui.css"/>
    
    <script src="/../treinamento/projeto/jquery/jquery-ui-1.12.1/jquery-ui.js"></script></head>
<body>
    <div class="container-fluid" id="container">
        <div class="row flex-row justify-content-start h-100">
            <aside class="col-12 p-0 mh-100 bg-primary">
                <nav class="navbar navbar-expand-lg navbar-light bg-primary border-bottom">
                    <a class="navbar-brand order-0 order-lg-0 mr-lg-0 mr-2" href="/../treinamento/projeto/view/indexFunc.php"><i class="fas fa-home mr-2"></i></a>
                    <a class="navbar-brand order-0 order-lg-0 ml-lg-0 ml-2 mr-auto ml-auto"><h4 class='text-light'>Olá, <?php echo $_SESSION['user']['user']; ?></h4></a>
                    <a class="navbar-brand order-0 order-lg-0 ml-lg-5 "><h4 class='text-light'>Editar Empresas</h4></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                            <li class="nav-item dropdown mr-4">
                                <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Cadastros
                                </a>
                                <div class="dropdown-menu bg-primary" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item text-light text-wrap" href="/../treinamento/projeto/view/lista.php?tipo=func">Funcionários</a>
                                <a class="dropdown-item text-light" href="/../treinamento/projeto/view/lista.php?tipo=cliente">Clientes</a>
                                <a class="dropdown-item text-light" href="/../treinamento/projeto/view/lista.php?tipo=empresa">Empresas</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown mr-4">
                                <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Chamados
                                </a>
                                <div class="dropdown-menu bg-primary" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item text-light text-wrap" href="/../treinamento/projeto/view/chamado/novoChamado.php">Abrir Chamado</a>
                                <a class="dropdown-item text-light text-wrap" href="/../treinamento/projeto/view/chamado/chamado.php">Gerenciar Chamados</a>
                                </div>
                            </li>
                            <li class="nav-item">
                            <a class="text-light nav-link" href="/../treinamento/projeto/logout.php">Sair</a> 
                            </li>
                        </ul>
                    </div>
                </nav>
            </aside>
            <div class="container-fluid">
                <div class="col-12">
                    <form id="atualizaEmpresa">
                        <input type="hidden" id="id" name="id" value="<?php echo $id?>" />
                        <div class="form-group offset-lg-3 col-lg-6 col-sm-12 pt-4 p-0">
                            <label for="nome">Nome:</label>
                            <input type="text" id="nome" name="nome" value="<?php echo $nome?>" class="form-control"/>
                        </div>
                        <div class="form-group offset-lg-3 col-lg-6 col-sm-12 pt-4 p-0">
                            <label for="cnpj">cnpj:</label>
                            <input type="text" id="cnpj" name="cnpj" value="<?php echo $cnpj?>" class="form-control"/>
                        </div>
                        </div>
                        <div class="form-group offset-lg-3 col-lg-6 col-sm-12 pt-3 text-right pr-0">
                            <button class="btn btn-danger" type="reset">Cancelar</button>
                            <button class="btn btn-primary "type="button" id="atualizarEmpresa">Enviar</button>
                        </div>
                        <div class="offset-lg-3 col-lg-6 col-sm-12 pt-3 pr-0 text-center" id="mensagem">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="/../treinamento/projeto/js/updateEmpresa.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="/../treinamento/projeto/js/bootstrap.bundle.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>


</body>
</html>