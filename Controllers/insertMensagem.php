<?php
require_once __DIR__.'/../session.php';
require_once __DIR__.'/../OO/Mensagem.php';
require_once __DIR__.'/MensagemControllers.php';
require_once __DIR__.'/../OO/Anexo.php';
require_once __DIR__.'/AnexoControllers.php';
$chamado = $_SESSION['chamado']['chamado'];
$user = $_SESSION['user']['user'];
require_once __DIR__.'/../conexao.php';

$mensagem = new Mensagem();

$i = 0;

if (!isset($FILES)) {
    $anexos = '';
}


$assuntoChamado = $conn->prepare("SELECT `assunto` FROM `projeto`.`chamado` WHERE `codigo` = '".$_SESSION['chamado']['chamado']."'");
$assuntoChamado->execute();
$assuntoChamado = $assuntoChamado->fetch(\PDO::FETCH_ASSOC);
$assuntoChamado = $assuntoChamado['assunto'];


$clienteId = $conn->prepare("SELECT `id_cliente` FROM `projeto`.`chamado` WHERE `codigo` = '".$_SESSION['chamado']['chamado']."'");
$clienteId->execute();
$resultCliente = $clienteId->fetch(\PDO::FETCH_ASSOC);
$cliente = $resultCliente['id_cliente'];

$cliente = $conn->prepare("SELECT `email` FROM `projeto`.`cliente` WHERE `codigo` = '$cliente'");
$cliente->execute();
$email = $cliente->fetch(\PDO::FETCH_ASSOC);


$assunto = "Resposta de Chamado CRM";
$texto = "Seu chamado de código '$chamado' referente à '$assuntoChamado' foi respondido";


$date = date('c');
if ($_POST['desc'] == '') {
    echo "<span class='text-danger lead font-weight-bold'>Envie uma mensagem</span>";
} else {
    $mensagem->setUser($user);
    $mensagem->setMensagem($_POST['desc']);
    $mensagem->setData($date);
    $mensagem->setChamado($chamado);
  
  
    $msg = new MensagemControllers($db);
    $files = new AnexoControllers($db);
    $anexo = new Anexo();
    
    $msg->salvar($mensagem, $anexo);
    foreach ($_FILES as $key) {
        $nome = $key['name'];
        $type = $key['type'];
        $anexos = explode('.', $key['name']);
        if ($anexos[1] != 'exe') {
            $anexos = file_get_contents($key['tmp_name']);
            $anexo->setNome($nome);
            $anexo->setMime($type);
            $anexo->setData($anexos);
            $files->salvar($anexo);
        } else {
            echo "<span class='text-danger lead font-weight-bold'>Proibido upload de arquivos potêncialmente maliciosos</span>";
        }
    
        $i++;
    }
    $clienteId = $conn->prepare("SELECT `id` FROM `projeto`.`cliente` WHERE `nomeUsuario` = '$user'");
    $clienteId->execute();
    $cliente = $clienteId->fetch(\PDO::FETCH_ASSOC);
    if ($cliente['id'] == '') {
        mail($email['email'], $assunto, $texto);
    }
    echo "<span class='text-success lead font-weight-bold'>Mensagem enviada</span>";
}
