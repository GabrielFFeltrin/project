$(document).ready(function () {
    $("#salvarFunc").click(function () {
        $.ajax({
            url: '/../treinamento/projeto/Controllers/insert.php',
            type: 'POST',
            data: {
                user: $("#user").val(),
                email: $("#email").val(),
                pass: $("#pass").val()
            },
            error: function (data) {
                $("#mensagem").html("Página não encontrada");
            },
            success: function (data) {
                $("#mensagem").html(data);
            }
        });
    });
});
