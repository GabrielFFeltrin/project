$(document).ready(function () {
    $("#anexo").change(function () {
        var data = addAnexo();
        $.ajax({
            url: '/../treinamento/projeto/Controllers/anexo.php',
            type: 'POST',
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {
                $("#imagem").html(data);
            }
        })
    });
    $("#salvarMensagem").click(function () {
        var anexo = addAnexo();
        anexo.append('desc', $("#desc").val())
        $.ajax({
            url: '/../treinamento/projeto/Controllers/insertMensagem.php',
            type: 'POST',
            data: anexo,

            processData: false,
            contentType: false,
            error: function (data) {
                $("#mensagem").html("Página não encontrada");
            },
            success: function (data) {
                $("#mensagem").html(data);
            }
        });
    });
    function addAnexo() {
        var data = new FormData();
        var anexo = document.getElementById("anexo");
        var anexos = anexo.files;
        if (anexos.length > 5) {
            alert('No máximo 5 arquivos por mensagem são permitidos, nenhum arquivo será enviado');
        } else {
            for (i = 0; i < anexos.length; i++) {

                data.append('anexo' + i, anexos[i]);
            }
        }
        return data;
    }
});