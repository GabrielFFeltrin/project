$(document).ready(function () {
    $("#foto").change(function () {
        $.ajax({
            url: '/../treinamento/projeto/Controllers/foto.php',
            type: 'POST',
            data: new FormData(document.getElementById('atualizaCliente')),
            processData: false,
            contentType: false,
            success: function (data) {
                $("#imagem").html(data);
            }
        })
    });
    $("#atualizarCliente").click(function () {
        $.ajax({
            url: '/../treinamento/projeto/Controllers/updateCliente.php',
            type: 'POST',
            data: new FormData(document.getElementById('atualizaCliente')),
            processData: false,
            contentType: false,
            error: function (data) {
                $("#mensagem").html("Página não encontrada");
            },
            success: function (data) {
                $("#mensagem").html(data);
            }
        });
    });
});