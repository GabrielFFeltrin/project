<?php

class ChamadoControllers
{
    private $db;

    public function __construct(Conn $db)
    {
        $this->db = $db->connect();
    }

    public function salvar(Chamado $chamado, MensagemControllers $msg, Mensagem $mensagem, Anexo $anexo)
    {
        $assunto = $chamado->getAssunto();
        $data = $chamado->getData();
        $status = $chamado->getStatus();
        $usuario = $chamado->getUsuario();
        $sqlInsert = $this->db->prepare("INSERT INTO `projeto`.`chamado` (`assunto`,`data`,`status`,`usuario`) VALUES ('$assunto','$data','$status','$usuario')");
        $sqlInsert->execute();

        $query = $this->db->prepare("SELECT `codigo` FROM `projeto`.`chamado` ORDER BY `codigo` DESC");
        $query->execute();
        $chamado = $query->fetchAll(PDO::FETCH_ASSOC);
        $chamado = $chamado[0]['codigo'];

        $mensagem->setChamado($chamado);
    }

    public function list($query, $cliente)
    {

        $sqlSearch = $this->db->prepare($query);
        $sqlSearch->execute();

        $lista = $sqlSearch->fetchAll(\PDO::FETCH_ASSOC);

        echo"   <table id='listTable' class='table table-hover'>
        
                <thead>
                    <tr>
                        <td>Código</td>
                        <td>Usuario</td>
                        <td>Assunto</td>
                        <td>Data</td>
                        <td>Status</td>
                        <td>Visualizar</td>";
                        if($cliente){ 
                            echo "<td>Excluir</td>";
                         } 
        echo"
                    </tr>
                </thead>
                <tbody>";
            if($lista == null){

                echo "</tbody></table><span class='text-danger lead font-weight-bold'>Nenhum registro encontrado</span>";
            }else{
                foreach ($lista as $row) {
                    $data = explode("-",$row['data']);
                    $aux = $data[0];
                    $data[0]=$data[2];
                    $data[2] = $aux;
                    $data = implode("/",$data);
                    if ($row['status'] == 0) {
                        $status = 'Aberto';
                    } else {
                        $status = 'Finalizado';
                    }
                    $codigo = $row['codigo'];
                    echo    "<tr>
                                <td> $codigo </td>
                                <td> ".$row['usuario']." </td>
                                <td> ".$row['assunto'] ."</td>
                                <td> ".$data." </td>
                                <td> $status </td>
                                <td> <a class='lead font-weight-bold' href='/../treinamento/projeto/view/chamado/visualiza.php?codigo=".$codigo."'>Visualizar</a></td>";
                                if($cliente){
                                    echo "<td><a onClick=\"javascript: return confirm('Confirma exclusão?');\" class='text-danger lead font-weight-bold' href='/../treinamento/projeto/Controllers/deleteChamado.php?id=".$codigo."'>Excluir</a></td>";
                                }
                    echo    "</tr>";
                }; 
            }
        echo "</tbody></table>";
    }

    public function finalizar($id)
    {
        $query = "UPDATE `projeto`.`chamado` set `status` = '1' WHERE `codigo` = '$id'";
        $query = $this->db->prepare($query);
        $query->execute();
        echo "<span class='text-success lead text-align-center font-weight-bold'>Chamado Finalizado</span>";
    }

    public function excluir($id, $anexo)
    {

        $select = "SELECT `id` FROM `projeto`.`mensagem` WHERE `id_chamado` = '$id'";
        $select = $this->db->prepare($select);
        $select->execute();
        $select = $select->fetchAll(PDO::FETCH_ASSOC);

        foreach ($select as $row) {
            $anexo->excluir($row['id']);
        }

        $query = "DELETE FROM `projeto`.`mensagem` WHERE `id_chamado` = '$id'";
        $query = $this->db->prepare($query);
        $query->execute();

        $query = "DELETE FROM `projeto`.`chamado` WHERE `codigo` = '$id'";
        $query = $this->db->prepare($query);
        $query->execute();

        header("Location: /../treinamento/projeto/view/chamado/chamado.php");
        
    }

    public function result($id, $cliente, $flag)
    {
        $query = $this->db->prepare("SELECT * FROM `projeto`.`chamado` WHERE `codigo` = '$id' AND `usuario` LIKE '%$cliente%'");
        $query->execute();
        $chamado = $query->fetch(PDO::FETCH_ASSOC);

        if ($chamado != null) {
            $data = explode("-", $chamado['data']);
            $aux = $data[0];
            $data[0]=$data[2];
            $data[2] = $aux;
            $data = implode("/", $data);
            echo"    
                
                <tbody>";
    
            if ($chamado['status'] == 0) {
                $status = 'Aberto';
            } else {
                $status = 'Finalizado';
            }
            $codigo = $chamado['codigo'];
            echo "  <tr>
                        <td> $codigo </td>
                        <td> ".$chamado['usuario']." </td>
                        <td> ".$chamado['assunto'] ."</td>
                        <td> ".$data." </td>
                        <td> $status </td>"; 
                        if ($flag) {
                            echo "<td> <a class='lead font-weight-bold' href='/../treinamento/projeto/view/chamado/visualiza.php?codigo=$codigo'>Visualizar</a></td>";
                        }
            echo"   </tr>";
        }
    }       
}
