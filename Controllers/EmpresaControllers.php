<?php


class EmpresaControllers
{
    private $db;
    private $empresa;

    public function __construct(Conn $db, Juridico $empresa)
    {
        $this->db = $db->connect();
        $this->empresa = $empresa;
    }

    public function salvar()
    {
        $nome = $this->empresa->getNome();
        $cnpj = $this->empresa->getCnpj();
        $sqlInsert = $this->db->prepare("INSERT INTO `projeto`.`empresa` (`cnpj`,`nome`) VALUES ('$cnpj','$nome')");
        $sqlInsert->execute();
    }

    public function update($id)
    {
        $nome = $this->empresa->getNome();
        $cnpj = $this->empresa->getCnpj();
        $sqlInsert = $this->db->prepare("UPDATE `projeto`.`empresa` SET `nome` = '$nome' ,`cnpj` = '$cnpj' WHERE `id` = '$id'");
        $sqlInsert->execute();
    }

    public function excluir($id)
    {
        $sqlDelete = $this->db->prepare("DELETE FROM `projeto`.`cliente` WHERE `id_empresa` = '$id'");
        $sqlDelete->execute();

        $sqlDelete = $this->db->prepare("DELETE FROM `projeto`.`empresa` WHERE `id` = '$id'");
        $sqlDelete->execute();

        header("Location: /../treinamento/projeto/view/lista.php?tipo=empresa");
    }
}
