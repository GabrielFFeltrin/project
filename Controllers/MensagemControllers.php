<?php


class MensagemControllers
{
    private $db;


    public function __construct(Conn $db)
    {
        $this->db = $db->connect();
    }

    public function salvar(Mensagem $mensagem, Anexo $anexo)
    {
        $mensagens = $mensagem->getMensagem();

        $data = $mensagem->getData();
        $chamado = $mensagem->getChamado();
        $user = $mensagem->getUser();
        $sqlInsert = $this->db->prepare("INSERT INTO `projeto`.`mensagem` (`id_chamado`,`mensagem`,`data`,`user`, `novo`) VALUES ('$chamado','$mensagens','$data','$user', 1)");
  
        $sqlInsert->execute();

        $query = $this->db->prepare("SELECT `id` FROM `projeto`.`mensagem` ORDER BY `id` DESC");
        $query->execute();
        $msg = $query->fetchAll(PDO::FETCH_ASSOC);
        $msg = $msg[0]['id'];

        
        $anexo->setMsg($msg);
    }

    public function mensagem($id, $anexo, $user)
    {
        $sqlSearch = $this->db->prepare("SELECT * FROM `projeto`.`mensagem` WHERE `id_chamado` = '$id' ORDER BY `data` DESC");
        $sqlSearch->execute();

        $lista = $sqlSearch->fetchAll(\PDO::FETCH_ASSOC);



        echo"    <thead>
                    <tr>
                        <td>Mensagem</td>
                        <td>Anexo</td>
                        <td>Data</td>
                        <td>Usuário</td>
                    </tr>
                 </thead>
                <tbody>";
        foreach ($lista as $row) {
            
            $sqlUpdate = $this->db->prepare("UPDATE `projeto`.`mensagem` SET `novo` = 0 WHERE `id_chamado` = '$id' AND `user` != '$user'");
            $sqlUpdate->execute();

            $data = explode("-",$row['data']);
            $aux = $data[0];
            $data[0]=$data[2];
            $data[2] = $aux;
            $data = implode("/",$data);
            $i = 0;
            echo "<tr>
        <td style='word-wrap: break-word'>".$row['mensagem']."</td>
        
            <td>";
            $anexo->anexo($row['id']);
            echo "</td>

        <td> ".$data."</td>
        <td> ".$row['user']."</td>
        </tr>";
        };
        echo "</tbody>";
    }
}
