<?php

include __DIR__ . '/../session.php';
include __DIR__ . '/../OO/Person.php';
include __DIR__ . '/../OO/Fisico.php';
include_once __DIR__ . '/FuncControllers.php';

$fisico = new Fisico();

$user = $_POST['user'];
$email = $_POST['email'];
$senha = $_POST['pass'];
$id = $_POST['id'];

require_once __DIR__.'/../conexao.php';

$validaEmail="SELECT `email` FROM `projeto`.`cliente` WHERE `email` = '$email'";
$validaEmail = $conn->prepare($validaEmail);
$validaEmail->execute();
$validaEmail1 = $validaEmail->fetch(PDO::FETCH_ASSOC);

$validaUser="SELECT `nomeUsuario` FROM `projeto`.`cliente` WHERE `nomeUsuario` = '$user'";
$validaUser = $conn->prepare($validaUser);
$validaUser->execute();
$validaUser1 = $validaUser->fetch(PDO::FETCH_ASSOC);


$validaEmail="SELECT `email` FROM `projeto`.`crmfunc` WHERE `email` = '$email' AND `id` != '$id'";
$validaEmail = $conn->prepare($validaEmail);
$validaEmail->execute();
$validaEmail2 = $validaEmail->fetch(PDO::FETCH_ASSOC);

$validaUser="SELECT `nomeUsuario` FROM `projeto`.`crmfunc` WHERE `nomeUsuario` = '$user' AND `id` != '$id'";
$validaUser = $conn->prepare($validaUser);
$validaUser->execute();
$validaUser2 = $validaUser->fetch(PDO::FETCH_ASSOC);



if ($user == '') {
    echo "<span class='text-danger lead font-weight-bold'>Nome obrigatório</span>";
} elseif ($validaUser1['nomeUsuario'] == $user || $validaUser2['nomeUsuario'] == $user) {
    echo "<span class='text-danger lead font-weight-bold'>Nome indisponível</span>";
} elseif ($email == '') {
    echo "<span class='text-danger lead font-weight-bold'>Email obrigatório</span>";
} elseif ($validaEmail1['email'] == $email || $validaEmail2['email'] == $email) {
    echo "<span class='text-danger lead font-weight-bold'>Email indisponível</span>";
} elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    echo "<span class='text-danger lead font-weight-bold'>Email inválido</span>";
} else {
    $fisico->setNome($user);
    $fisico->setEmail($email);
    if ($senha == '') {
        $senha="SELECT `password` FROM `projeto`.`crmfunc` WHERE `id` = '$id'";
        $senha = $conn->prepare($senha);
        $senha->execute();
        $senha = $senha->fetch(PDO::FETCH_ASSOC);
        $senha = $senha['password'];
    }
    $fisico->setSenha($senha);

    $func = new FuncControllers($db, $fisico);

    $func->update($id);
    echo "<span class='text-success lead font-weight-bold'>Atualizado com sucesso</span>";
    $_SESSION['user']['user'] = $user;
}
