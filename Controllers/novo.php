<?php
require_once __DIR__.'/../conexao.php';

require_once __DIR__.'/../session.php';
require_once __DIR__.'/../OO/Chamado.php';
require_once __DIR__.'/ChamadoControllers.php';

$user = $_SESSION['user']['user'];

$cliente       = $conn->prepare("SELECT * FROM `projeto`.`cliente` WHERE `nomeUsuario` = '".$_SESSION['user']['user']."'");
$cliente->execute();
$resultCliente = $cliente->fetch(\PDO::FETCH_ASSOC);
$func       = $conn->prepare("SELECT * FROM `projeto`.`crmfunc` WHERE `nomeUsuario` = '".$_SESSION['user']['user']."'");
$func->execute();
$resultFunc = $func->fetch(\PDO::FETCH_ASSOC);

$search = new ChamadoControllers($db);

$buscar = "SELECT * FROM `projeto`.`mensagem` WHERE `novo` = 1 AND `user` != '$user' ORDER BY `id_chamado`";

$sqlSearch = $conn->prepare($buscar);
$sqlSearch->execute();

$lista = $sqlSearch->fetchAll(\PDO::FETCH_ASSOC);

$id_chamado = '';
$i = 0;
foreach($lista as $row){
    if ($id_chamado != $row['id_chamado']) {
		$id_chamado = $row['id_chamado'];
        $buscar = "SELECT * FROM `projeto`.`chamado` WHERE `codigo` = '$id_chamado' AND `usuario` = '$user'";

        $sqlSearch = $conn->prepare($buscar);
		$sqlSearch->execute();
		$chamado = $sqlSearch->fetch(\PDO::FETCH_ASSOC);
		if($chamado != null){
			$i++;
		}
    }else{
		$id_chamado = $row['id_chamado'];
	}
	
}



echo "<h5 class='mt-3'>Você tem mensagens não visualizadas nos seguintes chamados: </h5><br>";
if($lista != null){
    if ($resultFunc != null) {
        $id_chamado = '';
        echo "<table class='table table-hover'>
		<thead>
				<tr>
					<td>Código</td>
					<td>Usuario</td>
					<td>Assunto</td>
					<td>Data</td>
					<td>Status</td>
					<td>Visualizar</td>
				</tr>
				</thead>";
        foreach ($lista as $row) {
            if ($row['user'] != $user) {
                if ($id_chamado != $row['id_chamado']) {
                    $search->result($row['id_chamado'], $resultCliente['nomeUsuario'], 1);
                    $id_chamado = $row['id_chamado'];
                }
                $id_chamado = $row['id_chamado'];
            }
        };
        echo "		</tbody>
				</table>";
    }elseif($i != 0){
		$id_chamado = '';
        echo "<table class='table table-hover'>
		<thead>
				<tr>
					<td>Código</td>
					<td>Usuario</td>
					<td>Assunto</td>
					<td>Data</td>
					<td>Status</td>
					<td>Visualizar</td>
				</tr>
				</thead>";
        foreach ($lista as $row) {
            if ($row['user'] != $user) {
                if ($id_chamado != $row['id_chamado']) {
                    $search->result($row['id_chamado'], $resultCliente['nomeUsuario'], 1);
                    $id_chamado = $row['id_chamado'];
                }
                $id_chamado = $row['id_chamado'];
            }
        };
        echo "		</tbody>
				</table>";
	}else{
		echo "<h5 class='mt-3'>Nenhuma mensagem pendente</h5>";
	}
}else{
	echo "<h5 class='mt-3'>Nenhuma mensagem pendente</h5>";
}



