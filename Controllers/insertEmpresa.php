<?php
include __DIR__ . '/../OO/Person.php';
include __DIR__ . '/../OO/Juridico.php';
include_once __DIR__ . '/EmpresaControllers.php';

$empresa = new Juridico();
$nome= $_POST['nome'];
$cnpj= $_POST['cnpj'];


require_once __DIR__.'/../conexao.php';


if ($nome == '') {
    echo "<span class='text-danger lead font-weight-bold'>Nome obrigatório</span>";
} elseif ($cnpj == '') {
    echo "<span class='text-danger lead font-weight-bold'>CNPJ obrigatório</span>";
} elseif ($cnpj == '00000000000000' ||
$cnpj == '11111111111111' ||
$cnpj == '22222222222222' ||
$cnpj == '33333333333333' ||
$cnpj == '44444444444444' ||
$cnpj == '55555555555555' ||
$cnpj == '66666666666666' ||
$cnpj == '77777777777777' ||
$cnpj == '88888888888888' ||
$cnpj == '99999999999999') {
    echo "<span class='text-danger lead font-weight-bold'>CNPJ inválido</span>";
} elseif (preg_match('/[^0-9]/', $cnpj)) {
    echo "<span class='text-danger lead font-weight-bold'>CNPJ inválido, apenas digitos permitidos</span>";
}elseif (strlen($cnpj) != 14) {
    echo "<span class='text-danger lead font-weight-bold'>CNPJ inválido</span>";
} else {
    $j = 5;
    $k = 6;
    $soma1 = 0;
    $soma2 = 0;

    for ($i = 0; $i < 13; $i++) {
        $j = $j == 1 ? 9 : $j;
        $k = $k == 1 ? 9 : $k;

        $soma2 += ($cnpj{$i} * $k);

        if ($i < 12) {
            $soma1 += ($cnpj{$i} * $j);
        }

        $k--;
        $j--;
    }

    $digito1 = $soma1 % 11 < 2 ? 0 : 11 - $soma1 % 11;
    $digito2 = $soma2 % 11 < 2 ? 0 : 11 - $soma2 % 11;

    if (($cnpj{12} == $digito1) and ($cnpj{13} == $digito2)) {
        $valida="SELECT `cnpj` FROM `projeto`.`empresa` WHERE `cnpj` = '$cnpj'";
        $valida = $conn->prepare($valida);
        $valida->execute();
        $valida = $valida->fetch(PDO::FETCH_ASSOC);
        if ($valida['cnpj'] == $cnpj) {
            echo "<span class='text-danger lead font-weight-bold'>CNPJ indisponível</span>";
        } else {
            $empresa->setCnpj($cnpj);
            $empresa->setNome($nome);
      
            $insert = new EmpresaControllers($db, $empresa);

            $insert->salvar($empresa);
            
            echo "<span class='text-success lead font-weight-bold'>Cadastrado com sucesso</span>";
        }
    } else {
        echo "<span class='text-danger lead font-weight-bold'>CNPJ inválido</span>";
    }
}
