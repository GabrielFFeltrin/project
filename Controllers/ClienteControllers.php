<?php
class ClienteControllers
{
    private $cliente;
    private $db;
    public function __construct(Conn $db, Cliente $cliente)
    {
        $this->db = $db->connect();
        $this->cliente = $cliente;
    }

    public function salvar()
    {
        $empresa = $this->cliente->getEmpresa();
        $foto = $this->cliente->getFoto();
        $nome = $this->cliente->getNome();
        $email = $this->cliente->getEmail();
        $senha = $this->cliente->getSenha();
        
        $insert = $this->db->prepare("INSERT INTO `projeto`.`cliente` (id_empresa,foto,nomeUsuario,email,password) VALUES ( '$empresa' , ? ,'$nome','$email','$senha')");
        $insert->bindParam(1, $foto);
        $insert->execute();
    }
    public function update($id)
    {
        $foto = $this->cliente->getFoto();
        $nome = $this->cliente->getNome();
        $email = $this->cliente->getEmail();
        $senha = $this->cliente->getSenha();

        $query = $this->db->prepare("UPDATE `projeto`.`cliente` set `foto` = ? ,`nomeUsuario` = '$nome',`email` = '$email',`password` = '$senha' WHERE `id` = '$id'");
        $query->bindParam(1, $foto);
        $query->execute();
    }

    public function excluir($id)
    {
        $sqlDelete = $this->db->prepare("DELETE FROM `projeto`.`cliente` WHERE `id` = '$id'");
        $sqlDelete->execute();

        header("Location: /../treinamento/projeto/view/lista.php?tipo=cliente");
    }
}
