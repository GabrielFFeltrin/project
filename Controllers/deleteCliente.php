<?php
require_once __DIR__.'/../conexao.php';

require_once __DIR__.'/../OO/Person.php';
require_once __DIR__.'/../OO/Fisico.php';
require_once __DIR__.'/../OO/Cliente.php';
require_once __DIR__.'/ClienteControllers.php';

$cliente = new Cliente();
$delete = new ClienteControllers($db, $cliente);

$id = $_GET['id'];

$delete->excluir($id);
