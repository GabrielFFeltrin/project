$(document).ready(function () {
    $("#atualizarFunc").click(function () {
        $.ajax({
            url: '/../treinamento/projeto/Controllers/updateFunc.php',
            type: 'POST',
            data: new FormData(document.getElementById('atualizaFunc')),
            processData: false,
            contentType: false,
            error: function (data) {
                $("#mensagem").html("Página não encontrada");
            },
            success: function (data) {
                $("#mensagem").html(data);
            }
        });
    });
});