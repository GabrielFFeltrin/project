CREATE TABLE `cliente` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`id_empresa` INT(11) NOT NULL,
	`foto` LONGBLOB NULL DEFAULT NULL,
	`nomeUsuario` VARCHAR(255) NOT NULL,
	`email` VARCHAR(100) NOT NULL,
	`password` VARCHAR(45) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `id_empresa` (`id_empresa`),
	CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id`)
);