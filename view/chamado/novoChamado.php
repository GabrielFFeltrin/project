<?php
require __DIR__.'/../../session.php';
require __DIR__.'/../../conexao.php';
$user = $_SESSION['user']['user'];

$func          = $conn->prepare("SELECT * FROM `projeto`.`crmfunc` WHERE `nomeUsuario` = '".$_SESSION['user']['user']."'");
$func->execute();
$resultFunc    = $func->fetch(\PDO::FETCH_ASSOC);
$cliente       = $conn->prepare("SELECT * FROM `projeto`.`cliente` WHERE `nomeUsuario` = '".$_SESSION['user']['user']."'");
$cliente->execute();
$resultCliente = $cliente->fetch(\PDO::FETCH_ASSOC);

if (!isset($_SESSION['user']['user'])) {
    echo "<script>alert('Acesso Negado')
    window.location.replace('/../treinamento/projeto/view/index.php')</script>";
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cadastro de Cliente</title>
    <link rel="stylesheet" href="/../treinamento/projeto/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="/../treinamento/projeto/downloads/fontAwesome/css/all.min.css"/>
    <script src="/../treinamento/projeto/jquery/jquery.js"></script>
    <link rel="stylesheet" href="/../treinamento/projeto/jquery/jquery-ui-1.12.1/jquery-ui.css"/>
    
    <script src="/../treinamento/projeto/jquery/jquery-ui-1.12.1/jquery-ui.js"></script>
</head>
<body>
    <div class="container-fluid" id="container">
        <div class="row flex-row justify-content-start h-100">
            <aside class="col-12 p-0 mh-100 bg-primary">
                <nav class="navbar navbar-expand-lg navbar-light bg-primary border-bottom">
                    <a class="navbar-brand order-0 order-lg-0 mr-lg-0 mr-2" href="/../treinamento/projeto/view/<?php if ($resultFunc != null) {
                                echo "indexFunc.php";
                            } elseif ($resultCliente != null) {
                                echo "indexCliente.php";
                            }?>"><i class="fas fa-home mr-2"></i></a>
                    <a class="navbar-brand order-0 order-lg-0 ml-lg-0 ml-2 mr-auto ml-auto"><h4 class='text-light'>Olá, <?php echo $_SESSION['user']['user']; ?></h4></a>
                    <a class="navbar-brand order-0 order-lg-0 ml-lg-5 "><h4 class='text-light'>Abrir Chamado</h4></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <?php if ($resultCliente['id'] == '') {
                            echo"
                            <li class='nav-item dropdown mr-4'>
                            <a class='nav-link dropdown-toggle text-light' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                            Cadastros
                            </a>
                            <div class='dropdown-menu bg-primary' aria-labelledby='navbarDropdown'>
                            <a class='dropdown-item text-light text-wrap' href='/../treinamento/projeto/view/lista.php?tipo=func'>Funcionários</a>
                            <a class='dropdown-item text-light' href='/../treinamento/projeto/view/lista.php?tipo=cliente'>Clientes</a>
                            <a class='dropdown-item text-light' href='/../treinamento/projeto/view/lista.php?tipo=empresa'>Empresas</a>
                            </div>
                        </li>";
                        }else{
                            echo"
                            <li class='nav-item dropdown mr-4'>
                                <a class='nav-link text-light text-wrap' href='/../treinamento/projeto/view/cliente/editarCliente.php?id=".$resultCliente['id']."'>Editar Perfil</a>
                            </li>";
                        }?>
                            <li class="nav-item dropdown mr-4">
                                <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Chamados
                                </a>
                                <div class="dropdown-menu bg-primary" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item text-light text-wrap" href="/../treinamento/projeto/view/chamado/novoChamado.php">Abrir Chamado</a>
                                <a class="dropdown-item text-light text-wrap" href="/../treinamento/projeto/view/chamado/<?php if ($resultFunc != null) {
                                        echo "chamado.php";
                                    } elseif ($resultCliente != null) {
                                        echo "chamadoCliente.php";
                                    }?>">Chamados</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="text-light nav-link" href="/../treinamento/projeto/logout.php">Sair</a> 
                            </li>
                        </ul>
                    </div>
                </nav>
            </aside>
            <div class="container-fluid">
                <div class="col-12">
                    <form id ="chamado">
                        <div class='row'>
                            <div class='row-horizontal col-lg-5 col-sm-12 mt-3 p-0'>            
                                <div class="form-group col-lg-12 col-sm-12 p-0">
                                    <label for="assunto">Assunto:</label>
                                    <input type="text" id="assunto" name="assunto" placeholder="Assunto" class="form-control"/>
                                </div>
                                <div class="form-group col-lg-12 col-sm-12  p-0">
                                    <label for="desc">Descrição/Mensagem:</label>
                                    <textarea id="desc" name="desc" rows="6" cols="50" placeholder="Mensagem" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group col-lg-5 col-sm-12 ml-5 mt-5 custom-file">
                                <label class="custom-file-label" for="anexo[]">Escolher arquivo</label>
                                <input type="file" class="custom-file-input" id="anexo" name="anexo[]" multiple="multiple">
                                <div id="imagem" class='row'></div>
                            </div>
                            <div class="form-group col-lg-5 col-sm-12 pt-3 text-left ">
                                <button class="btn btn-danger" type="reset">Cancelar</button>
                                <button class="btn btn-primary "type="button" id="salvarChamado">Enviar</button>
                            </div>

                            <div class=" col-lg-12 col-sm-12 pt-5 pr-0 text-center" id="mensagem">
                            </div>
                        </div>
                    </form>
                </div>  
            </div>
        </div>
    </div>
    <script src="/../treinamento/projeto/js/novoChamado.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="/../treinamento/projeto/js/bootstrap.bundle.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>


</body>
</html>