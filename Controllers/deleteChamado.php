<?php
require_once __DIR__.'/../conexao.php';

require_once __DIR__.'/../OO/Chamado.php';
require_once __DIR__.'/ChamadoControllers.php';
require_once __DIR__.'/AnexoControllers.php';


$chamado = new Chamado();
$delete = new ChamadoControllers($db, $chamado);
$anexo = new AnexoControllers($db);

$id = $_GET['id'];

$delete->excluir($id, $anexo);

