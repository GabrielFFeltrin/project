$(document).ready(function () {
	var chamado = $("#codigo").val();
	$.ajax({
		url: '/../treinamento/projeto/Controllers/mensagem.php',
		type: 'POST',
		data: {
			chamado: chamado,
		},
		error: function (data) {
			$("#mensagem").html("Página não encontrada");
		},
		success: function (data) {
			$("#mensagens").html(data);
		},
	});

	$("#voltar").click(function () {
		window.location.href='/../treinamento/projeto/view/chamado/visualiza.php?codigo='+chamado;
	});
});